/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author ทักช์ติโชค
 */
public class CircleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Circle");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(null);

        JLabel lblRadius = new JLabel("radius:", JLabel.TRAILING);
        lblRadius.setSize(40, 20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        frame.add(lblRadius);

        final JTextField txtRadius = new JTextField();
        txtRadius.setSize(50, 20);
        txtRadius.setLocation(50, 5);
        frame.add(txtRadius);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(80, 40);
        btnCalculate.setBackground(Color.YELLOW);
        btnCalculate.setOpaque(true);
        frame.add(btnCalculate);

        final JLabel lblResuit = new JLabel("Circle radius= ??? area= ??? perimeter= ???", JLabel.CENTER);
        lblResuit.setHorizontalAlignment(JLabel.CENTER);
        lblResuit.setSize(300, 50);
        lblResuit.setLocation(0, 80);
        lblResuit.setBackground(Color.GREEN);
        lblResuit.setOpaque(true);
        frame.add(lblResuit);

        btnCalculate.addActionListener(new ActionListener() { //Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strRadius = txtRadius.getText(); //1. ดึงข้อมูล txt จาก txtRadius -> strRadius
                    double radius = Double.parseDouble(strRadius); //2. แปลง strRadius -> radius: double pasreDouble
                    Circle circle = new Circle(radius); //3. instance objeact Circle(radius) -> circle
                    lblResuit.setText("Circle r = " + String.format("%.2f", circle.getRadius())//4. update lblResuit โดยนำข้อมูลจาก circle ไปแสดงให้ครบถ้วน
                            + " area = " + String.format("%.2f", circle.calArea())
                            + " perimeter = " + String.format("%.2f", circle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Erorr: Plase input number", "Erorr"
                            , JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
            }

        });
    }

}
