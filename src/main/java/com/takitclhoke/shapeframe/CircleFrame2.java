/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ทักช์ติโชค
 */
public class CircleFrame2 extends JFrame {

    JLabel lblRadius;
    JTextField txtRadius;
    JButton btnCalculate;
    JLabel lblResuit;

    public CircleFrame2() {
        super("Circle");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblRadius = new JLabel("radius:", JLabel.TRAILING);
        lblRadius.setSize(40, 20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        this.add(lblRadius);

        txtRadius = new JTextField();
        txtRadius.setSize(50, 20);
        txtRadius.setLocation(50, 5);
        this.add(txtRadius);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(80, 40);
        btnCalculate.setBackground(Color.YELLOW);
        btnCalculate.setOpaque(true);
        this.add(btnCalculate);

        lblResuit = new JLabel("Circle radius= ??? area= ??? perimerer= ???", JLabel.CENTER);
        lblResuit.setHorizontalAlignment(JLabel.CENTER);
        lblResuit.setSize(300, 50);
        lblResuit.setLocation(0, 80);
        lblResuit.setBackground(Color.GREEN);
        lblResuit.setOpaque(true);
        this.add(lblResuit);

        btnCalculate.addActionListener(new ActionListener() { //Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strRadius = txtRadius.getText(); //1. ดึงข้อมูล txt จาก txtRadius -> strRadius
                    double radius = Double.parseDouble(strRadius); //2. แปลง strRadius -> radius: double pasreDouble
                    Circle circle = new Circle(radius); //3. instance objeact Circle(radius) -> circle
                    lblResuit.setText("Circle r = " + String.format("%.2f", circle.getRadius())//4. update lblResuit โดยนำข้อมูลจาก circle ไปแสดงให้ครบถ้วน
                            + " area = " + String.format("%.2f", circle.calArea())
                            + " perimeter = " + String.format("%.2f", circle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(CircleFrame2.this, "Erorr: Plase input number", "Erorr",
                            JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
            }

        });
    }

    public static void main(String[] args) {
        CircleFrame2 frame = new CircleFrame2();
        frame.setVisible(true);
    }
}
