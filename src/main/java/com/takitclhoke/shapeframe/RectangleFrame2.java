/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ทักช์ติโชค
 */
public class RectangleFrame2 extends JFrame {

    JLabel lblWidth;
    JTextField txtWidth;
    JLabel lblHeigth;
    JTextField txtHeigth;
    JButton btnCalculate;
    JLabel lblResuit;

    public RectangleFrame2() {

        this.setSize(500, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblWidth = new JLabel("width:", JLabel.TRAILING);
        lblWidth.setSize(50, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        this.add(lblWidth);

        txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(50, 5);
        this.add(txtWidth);

        lblHeigth = new JLabel("heigth:", JLabel.TRAILING);
        lblHeigth.setSize(50, 20);
        lblHeigth.setLocation(100, 5);
        lblHeigth.setBackground(Color.WHITE);
        lblHeigth.setOpaque(true);
        this.add(lblHeigth);

        txtHeigth = new JTextField();
        txtHeigth.setSize(50, 20);
        txtHeigth.setLocation(150, 5);
        this.add(txtHeigth);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(80, 40);
        btnCalculate.setBackground(Color.CYAN);
        btnCalculate.setOpaque(true);
        this.add(btnCalculate);

        lblResuit = new JLabel("Rectangle width= ??? heigth= ??? area= ??? perimeter= ???");
        lblResuit.setHorizontalAlignment(JLabel.CENTER);
        lblResuit.setSize(500, 50);
        lblResuit.setLocation(0, 80);
        lblResuit.setBackground(Color.BLUE);
        lblResuit.setOpaque(true);
        this.add(lblResuit);

        btnCalculate.addActionListener(new ActionListener() { //Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidth = txtWidth.getText(); //1. ดึงข้อมูล txt จาก txtWidth -> strWidth
                    String strHigth = txtHeigth.getText(); //1.1 ดึงข้อมูล txt จาก txtHeigth -> strHeigth
                    double width = Double.parseDouble(strWidth); //2. แปลง strWidth -> width: double pasreDouble
                    double higth = Double.parseDouble(strHigth); //2. แปลง strHeigth -> heigth: double pasreDouble
                    Rectangle rectangle = new Rectangle(width, higth); //3. instance objeact Recrangle(width,higth) -> rectangle                 
                    //4. update lblResuit โดยนำข้อมูลจาก Rectangle ไปแสดงให้ครบถ้วน
                    lblResuit.setText("Rectangle width = " + String.format("%.2f", rectangle.getWidth())
                            + " higth = " + String.format("%.2f", rectangle.getHeight())
                            + " area = " + String.format("%.2f", rectangle.calArea())
                            + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame2.this, "Erorr: Plase input number", "Erorr",
                            JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtWidth.requestFocus();
                    txtHeigth.setText("");
                    txtHeigth.requestFocus();
                }
            }

        });
    }

    public static void main(String[] args) {
        RectangleFrame2 frame = new RectangleFrame2();
        frame.setVisible(true);
    }

}
