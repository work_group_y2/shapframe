/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ทักช์ติโชค
 */
public class SquareFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Square");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setVisible(true);

        JLabel lblSide = new JLabel("side:", JLabel.TRAILING);
        lblSide.setSize(40, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        frame.add(lblSide);

        final JTextField txtSide = new JTextField();
        txtSide.setSize(40, 20);
        txtSide.setLocation(50, 5);
        frame.add(txtSide);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(80, 40);
        btnCalculate.setBackground(Color.PINK);
        btnCalculate.setOpaque(true);
        frame.add(btnCalculate);

        final JLabel lblResuit = new JLabel("Square side= ??? area= ??? perimeter= ???");
        lblResuit.setHorizontalAlignment(JLabel.CENTER);
        lblResuit.setSize(300, 50);
        lblResuit.setLocation(0, 80);
        lblResuit.setBackground(Color.RED);
        lblResuit.setOpaque(true);
        frame.add(lblResuit);

        btnCalculate.addActionListener(new ActionListener() { //Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strSide = txtSide.getText(); //1. ดึงข้อมูล txt จาก txtSide-> strSide
                    double side = Double.parseDouble(strSide); //2. แปลง strSide -> side: double pasreDouble
                    Square square = new Square(side); //3. instance objeact Square(side) -> circle
                    lblResuit.setText("Square side = " + String.format("%.2f", square.getSide())//4. update lblResuit โดยนำข้อมูลจาก square ไปแสดงให้ครบถ้วน
                            + " area = " + String.format("%.2f", square.calArea())
                            + " perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Erorr: Plase input number", "Erorr",
                             JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }

        });

    }

}
