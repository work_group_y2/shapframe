/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ทักช์ติโชค
 */
public class SquareFrame2 extends JFrame {

    JLabel lblSide;
    JTextField txtSide;
    JButton btnCalculate;
    JLabel lblResuit;

    public SquareFrame2() {
        super("Square");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);


        lblSide = new JLabel("side:", JLabel.TRAILING);
        lblSide.setSize(40, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        this.add(lblSide);

        txtSide = new JTextField();
        txtSide.setSize(40, 20);
        txtSide.setLocation(50, 5);
        this.add(txtSide);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(80, 40);
        btnCalculate.setBackground(Color.PINK);
        btnCalculate.setOpaque(true);
        this.add(btnCalculate);

        lblResuit = new JLabel("Square side= ??? area= ??? perimeter= ???");
        lblResuit.setHorizontalAlignment(JLabel.CENTER);
        lblResuit.setSize(300, 50);
        lblResuit.setLocation(0, 80);
        lblResuit.setBackground(Color.RED);
        lblResuit.setOpaque(true);
        this.add(lblResuit);

        btnCalculate.addActionListener(new ActionListener() { //Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strSide = txtSide.getText(); //1. ดึงข้อมูล txt จาก txtRadius -> strRadius
                    double side = Double.parseDouble(strSide); //2. แปลง strRadius -> radius: double pasreDouble
                    Square square = new Square(side); //3. instance objeact Circle(radius) -> circle
                    lblResuit.setText("Square side = " + String.format("%.2f", square.getSide())//4. update lblResuit โดยนำข้อมูลจาก circle ไปแสดงให้ครบถ้วน
                            + " area = " + String.format("%.2f", square.calArea())
                            + " perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(SquareFrame2.this, "Erorr: Plase input number", "Erorr",
                            JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }

        });
    }

    public static void main(String[] args) {
        SquareFrame2 frame = new SquareFrame2();
        frame.setVisible(true);
    }

}
