/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.shapeframe;

/**
 *
 * @author ทักช์ติโชค
 */
public class Triangle extends Shape {

    private double base;
    private double heigth;
    private double side;

    public Triangle(double base, double heigth, double side) {
        super("Triangle");
        this.base = base;
        this.heigth = heigth;
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getHeigth() {
        return heigth;
    }

    public void setHeigth(double heigth) {
        this.heigth = heigth;
    }

    @Override
    public double calArea() {
        return (base * heigth) / 2.0;
    }

    @Override
    public double calPerimeter() {
        return base + heigth + side;
    }

}
