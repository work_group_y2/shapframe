/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ทักช์ติโชค
 */
public class TriangleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Triangle");
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(null);

        JLabel lblBase = new JLabel("base:", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        frame.add(lblBase);

        final JTextField txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);
        frame.add(txtBase);

        JLabel lblHeigth = new JLabel("heigth:", JLabel.TRAILING);
        lblHeigth.setSize(50, 20);
        lblHeigth.setLocation(100, 5);
        lblHeigth.setBackground(Color.WHITE);
        lblHeigth.setOpaque(true);
        frame.add(lblHeigth);

        final JTextField txtHeigth = new JTextField();
        txtHeigth.setSize(50, 20);
        txtHeigth.setLocation(150, 5);
        frame.add(txtHeigth);

        JLabel lblSide = new JLabel("side:", JLabel.TRAILING);
        lblSide.setSize(50, 20);
        lblSide.setLocation(200, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        frame.add(lblSide);

        final JTextField txtSide = new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(250, 5);
        frame.add(txtSide);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(80, 40);
        btnCalculate.setBackground(Color.GRAY);
        btnCalculate.setOpaque(true);
        frame.add(btnCalculate);

        final JLabel lblResuit = new JLabel("Triangle base= ??? heigth= ??? side= ??? area= ??? perimeter= ???");
        lblResuit.setHorizontalAlignment(JLabel.CENTER);
        lblResuit.setSize(500, 50);
        lblResuit.setLocation(0, 80);
        lblResuit.setBackground(Color.ORANGE);
        lblResuit.setOpaque(true);
        frame.add(lblResuit);

        btnCalculate.addActionListener(new ActionListener() { //Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText(); //1. ดึงข้อมูล txt จาก txtBase -> strBase
                    String strHigth = txtHeigth.getText(); //1.1 ดึงข้อมูล txt จาก txtHeigth -> strHeigth
                    String strSide = txtSide.getText(); //1.2 ดึงข้อมูล txt จาก txtSide -> strSide
                    double base = Double.parseDouble(strBase); //2. แปลง strBase -> base: double pasreDouble
                    double higth = Double.parseDouble(strHigth); //2. แปลง strHeigth -> heigth: double pasreDouble
                    double side = Double.parseDouble(strSide); //2. แปลง strSide -> side: double pasreDouble
                    Triangle triangle = new Triangle(base, higth, side); //3. instance objeact Triangle(base,higth,side) -> Triangle                 
                    //4. update lblResuit โดยนำข้อมูลจาก Triangle ไปแสดงให้ครบถ้วน
                    lblResuit.setText("Trangle base = " + String.format("%.2f", triangle.getBase())
                            + " higth = " + String.format("%.2f", triangle.getHeigth())
                            + " side = " + String.format("%.2f", triangle.getSide())
                            + " area = " + String.format("%.2f", triangle.calArea())
                            + " perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Erorr: Plase input number", "Erorr",
                            JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtBase.requestFocus();
                    txtHeigth.setText("");
                    txtHeigth.requestFocus();
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }

        });
    }
}
